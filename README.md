# Installasi
npm install --untuk install package yang dibutuhkan

# Running Code
ng serve --untuk start atau menjalankan aplikasi default http://localhost:4200/auth/login

# Password Login
Username = admin123
Password = 12345

# Versi angular
Angular CLI: 12.2.18

# Versi Node
Node: 12.22.3
Package Manager: npm 6.14.13

# DEMO WITH VERCEL
link: https://employee-management-satria.vercel.app/auth/login
Username = admin123
Password = 12345