import * as version from '../../package.json';

export const environment = {
  production: true,
  VERSION: version,
  getList: 'https://65a238c542ecd7d7f0a74150.mockapi.io/s1/OneIndo',
  SECRET_KEY: 'arg'
};
