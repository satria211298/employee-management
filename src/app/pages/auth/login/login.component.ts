import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DummyDataUser } from 'src/app/consts/dummy-login-success';
import { DummyLoginFailed } from 'src/app/consts/dummy-login-failed';
import { ResponseLogin } from 'src/app/models/response.model';
import { LoadingService } from 'src/app/shared/loader/loading.service';
import { ToastService } from 'src/app/shared/notif/toast.service';
import { credential } from 'src/app/lib/security';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  hide: boolean = true;
  errorMessage: string = "";
  userActive?: ResponseLogin;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private loading: LoadingService,
    private toast: ToastService
  ) { 
    this.userActive = DummyDataUser;
    this.toast.sizeScreen();
    let emoji: any = Validators.pattern(/[\u0020-\u007e\u00a0-\u00ff\u0152\u0153\u0178]/);
    this.form = this.formBuilder.group(
      {
        username: ['', [emoji, Validators.required]],
        password: ['', [emoji, Validators.required]]
      },
    );
  }

  ngOnInit(): void {}

  onSubmit() {
    this.errorMessage = '';
    if(this.form.invalid) {
      return;
    }else {
      this.loading.showLoading(true);
      setTimeout(() => {
        if(this.form.value.username === this.userActive?.result?.data.resultUserProfileHeader.nik && this.form.value.password === this.userActive?.result?.data.resultUserProfileHeader.password) {
          credential.storage.set('user', JSON.stringify(this.userActive?.result?.data));
          credential.storage.set('menu', JSON.stringify(this.userActive?.result?.data.resultUserProfileMenu));
          this.loading.showLoading(false);
          const wait = async () => {
            await this.toast.notif("success", "Login");
            this.router.navigate(['/dashboard/layout']);
          }
          wait();
        }else {
          this.loading.showLoading(false);
          this.errorMessage = DummyLoginFailed.status.responseDesc;
        }
      }, 1500);
    }
  }

}
