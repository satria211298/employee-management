import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

interface listGrp {
  name: string;
  value: string;
}

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  form!: FormGroup;
  lahir: any;
  bulan: any;
  tempTanggalLahir: Date = new Date();
  tanggalLahir: string = '';
  maxDate: Date = new Date();
  lisGrp: listGrp[] = [];
  filteredGroup: any = this.lisGrp;
  flag: string = '';

  group_data = [
    { "id": 1, "name": "Liaison" },
    { "id": 2, "name": "Planner" },
    { "id": 3, "name": "Representative" },
    { "id": 4, "name": "Specialist" },
    { "id": 5, "name": "Representative" },
    { "id": 6, "name": "Producer" },
    { "id": 7, "name": "Coordinator" },
    { "id": 8, "name": "Developer" },
    { "id": 9, "name": "Associate" },
    { "id": 10, "name": "Executive" },
    { "id": 11, "name": "Assistant" },
    { "id": 12, "name": "Administrator" },
    { "id": 13, "name": "Producer" },
    { "id": 14, "name": "Representative" },
    { "id": 15, "name": "Planner" },
    { "id": 16, "name": "Agent" },
    { "id": 17, "name": "Specialist" },
    { "id": 18, "name": "Architect" },
    { "id": 19, "name": "Strategist" },
    { "id": 20, "name": "Designer" },
    { "id": 21, "name": "Orchestrator" },
    { "id": 22, "name": "Administrator" },
    { "id": 23, "name": "Facilitator" },
    { "id": 24, "name": "Director" },
    { "id": 25, "name": "Manager" },
    { "id": 26, "name": "Consultant" },
    { "id": 27, "name": "Officer" },
    { "id": 28, "name": "Supervisor" },
    { "id": 29, "name": "Strategist" },
    { "id": 30, "name": "Analyst" },
    { "id": 31, "name": "Technician" },
    { "id": 32, "name": "Engineer" }
  ]

  @ViewChild('picker2') picker2: any;

  constructor
    (
      private formBuilder: FormBuilder,
      private router: Router,
      private route: ActivatedRoute
    ) { this.validateInput(); }

  ngOnInit(): void {
    this.getGroup();
    this.getDetailEmpl();

  }

  onSubmit() {
    if (this.form.valid) {
      const ToastTwo = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2500,
        width: 350,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      ToastTwo.fire({
        icon: 'success',
        title: 'Berhail Menyimpan Data!'
      })
    }
  }

  getDetailEmpl() {
    this.route.queryParams.subscribe(param => {
      this.flag = param.flag
      console.log(param.flag)
      if (param.flag == 'TOP') {
        console.log(param.basicSallary)
        this.form.controls.username.setValue(param.username);
        this.form.controls.firstName.setValue(param.firstName);
        this.form.controls.lastName.setValue(param.lastName);
        this.form.controls.email.setValue(param.email);
        this.form.controls.birthDate.setValue(param.birthDate);
        const cek = new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(param.basicSallary);
        this.form.controls.basicSalary.setValue(cek);
        this.form.controls.status.setValue(param.status);
        this.form.controls.group.setValue(param.group);
        this.form.controls.description.setValue(param.description);
        this.form.controls['username'].disable();
        this.form.controls['firstName'].disable();
        this.form.controls['lastName'].disable();
        this.form.controls['email'].disable();
        this.form.controls['birthDate'].disable();
        this.form.controls['basicSalary'].disable();
        this.form.controls['status'].disable();
        this.form.controls['group'].disable();
        this.form.controls['description'].disable();
      }
    })
  }

  getGroup() {
    let arrayListGroup: listGrp[] = [];
    this.group_data.forEach((element: any) => {
      arrayListGroup.push({ name: element.name, value: element.id })
    })
    this.lisGrp = arrayListGroup;
    this.filteredGroup = this.lisGrp;
  }

  validateInput() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required, Validators.pattern(/^\S+$/)]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email, Validators.pattern(/[\u0020-\u007e\u00a0-\u00ff\u0152\u0153\u0178]/)]],
      birthDate: ['', [Validators.required]],
      basicSalary: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      status: ['', [Validators.required]],
      group: ['', [Validators.required]],
      description: ['', [Validators.required]]
    });
  }

  addEventAwal(type: string, event: MatDatepickerInputEvent<Date>) {
    this.tempTanggalLahir = new Date(`${type}: ${event.value}`);
    this.lahir = this.tempTanggalLahir;

    let getBulan = this.lahir.getMonth();
    let namaBulan = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    getBulan = namaBulan[getBulan];
    this.bulan = getBulan;

    this.tanggalLahir = (('0' + this.tempTanggalLahir.getDate()).slice(-2) + '-' + (this.bulan) + '-' + this.tempTanggalLahir.getFullYear()).toString()
  }

  open() {
    this.picker2.open();
  }

  navigateListEmpl() {
    this.router.navigate(['/list-employee'], {
    });
  }

}
