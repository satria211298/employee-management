import { Component, OnInit, ViewChild, AfterViewInit, ViewChildren, QueryList, Input } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ErrorRequestService } from 'src/app/shared/handle-error/error-request.service';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { listEmployee } from 'src/app/models/list-employee.model';
import { Router } from '@angular/router';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { catchError } from 'rxjs/operators';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { LoadingService } from "src/app/shared/loader/loading.service";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

const httpOptions: Object = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
  observe: 'response',
  responseType: 'json',
};

@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.css']
})
export class ListEmployeeComponent implements OnInit {

  displayedColumnsResult: string[] = ['no', 'username', 'firstName', 'lastName', 'email', 'birthDate', 'basicSallary', 'status', 'group', 'description', 'detail', 'action'];
  dataSource!: MatTableDataSource<listEmployee>;
  newEmployeeList: listEmployee[] = [];
  showClass: boolean = false;

  //tabel
  isLoading = true;
  searchData: string = '';
  listEmployee: listEmployee[] = [];

  @ViewChild('paginator')
  paginator!: MatPaginator;

  @ViewChild('sort')
  sort!: MatSort;

  constructor
    (
      private http: HttpClient,
      private router: Router,
      private route: ActivatedRoute,
      private handleError: ErrorRequestService,
      private breakpointObserver: BreakpointObserver,
      public dialog: MatDialog,
      private toggleLoading: LoadingService,
    ) {
    this.dataSource = new MatTableDataSource(this.newEmployeeList);
  }

  ngOnInit(): void {
    this.getListEmployee()
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  new(catchError: any): Observable<HttpResponse<any>> {
    return this.http.get<any>(environment.getList, httpOptions).pipe(catchError);
  }

  // drag and drop table
  onColumnDrop(event: CdkDragDrop<string[], string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(this.displayedColumnsResult, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      this.displayedColumnsResult.splice(event.currentIndex, 0, event.item.data);
    }
  }

  getListEmployee() {
    const ToastTwo = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 2500,
      width: 350,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })

    this.isLoading = true;
    this.listEmployee = [];
    this.dataSource = new MatTableDataSource(this.newEmployeeList)
    this.new(catchError(this.handleError.handleError.bind(this))).subscribe(result => {
      if (result.body.length > 0 && result.status == 200) {
        result.body.forEach((element: any) => {
          this.listEmployee.push({
            no: element.id,
            username: element.username,
            firstName: element.firstName,
            lastName: element.lastName,
            email: element.email,
            birthDate: element.birthDate,
            basicSallary: element.basicSallary,
            status: element.status,
            group: element.group,
            description: element.description
          })
        })
        this.dataSource = new MatTableDataSource(this.listEmployee);
        this.ngAfterViewInit();
        this.isLoading = false;
      } else if (result.body.length < 1 && result.status == 200) {
        ToastTwo.fire({
          icon: 'info',
          title: 'Tidak Ada Data Yang Ditemukan!'
        })
        this.isLoading = false;
      } else {
        ToastTwo.fire({
          icon: 'error',
          title: 'Terjadi Kesalahan Dalam Memuat Data!'
        })
        this.isLoading = false;
      }
    })
  }

  searchResult() {
    this.isLoading = true;
    const filterValue = this.searchData;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
      this.isLoading = false;
    } else {
      this.isLoading = false;
    }
  }

  onSearchChange() {
    if (this.searchData == '') {
      this.getListEmployee();
    }
  }

  deleteData() {
    const ToastTwo = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 2500,
      width: 350,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    ToastTwo.fire({
      icon: 'error',
      title: 'Berhail Mengapus Data!'
    })
  }

  editData() {
    const ToastTwo = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 2500,
      width: 350,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    ToastTwo.fire({
      icon: 'warning',
      title: 'Berhail Update Data!'
    })
  }

  navigateAddEmpl() {
    this.router.navigate(['/add-employee'], {
    });
  }

  navigateDetailEmpl(data: any) {
    this.router.navigate(['/add-employee/'], {
      queryParams: {
        username: data.username,
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        birthDate: data.birthDate,
        basicSallary: data.basicSallary,
        status: data.status,
        group: data.group,
        description: data.description,
        flag: 'TOP'
      },
    });
  }
}
