import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";

import { DashboardRoutingModule } from './dashboard-routing.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { DashboardLayoutComponent } from './dashboard-layout/dashboard-layout.component';
import { AvatarDialog, SidebarComponent } from './sidebar/sidebar.component';
import { ConfirmLogout, HeaderComponent, ProfilDialog } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    DashboardLayoutComponent,
    SidebarComponent,
    HeaderComponent,
    ProfilDialog,
    ConfirmLogout,
    HomeComponent,
    AvatarDialog
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    HttpClientModule
  ]
})
export class DashboardModule { }
