import { HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { throwError } from "rxjs";
import { LoadingService } from "../loader/loading.service";
import Swal from 'sweetalert2'

@Injectable({
    providedIn: 'root'
})
export class ErrorRequestService {

  constructor(
    private toggleLoading: LoadingService,
    private router: Router
) {}
isLoading = true;
// handle error get Search Employee
handleError(error: HttpErrorResponse) {
    console.log(error);
    // this.toggleLoading.showLoading(false);
    const ToastTwo = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2500,
        width: 350,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    
    if (error.status === 0) {
    // A client-side or network error occurred. Handle it accordingly.
    console.error('An error occurred:', error.error);
        ToastTwo.fire({
            icon: 'info',
            title: 'Service Time-out!'
        })
        this.toggleLoading.showLoading(false);
        this.isLoading = false;
    } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong.
    console.error(`Backend returned code ${error.status}, body was: `, error.error);
        ToastTwo.fire({
            icon: 'error',
            title: '404 Service Not Found!'
        })
        this.toggleLoading.showLoading(false);
        this.isLoading = false;
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
}

}
