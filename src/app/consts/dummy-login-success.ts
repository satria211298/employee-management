import { ResponseLogin } from "../models/response.model";

export const DummyDataUser: ResponseLogin = {
    status: {
        responseCode: 200,
        responseDesc: "Login Success"
    },
    result: {
        status: true,
        data: {
            resultUserProfileHeader: {
                nik: "admin123",
                password: "12345",
                fullname: "Satria Ramadhan",
                parent_nik: "123456789",
                hp_no: "081314462819",
                email: "satria@gmail.com",
                portfolio_desc: "",
                last_login_date: "Januari 2024"
            },
            resultProfileUserRole: [
                {
                    role_code: "MGR"
                }
            ],
            resultUserProfileLocation: [
                {
                    branch_code: "0000",
                    branch_name: "Tangerang",
                    location_code: "0SR0",
                    location_name: "Rajeg"
                }
            ],
            resultUserProfileJob: [
                {
                    job_code: "01",
                    job_desc: "Manager"
                }
            ],
            resultUserProfileMenu: [
                {
                    menuCode: "5101",
                    menuName: "Employee Management",
                    details: [
                        {
                            "functionCode": "view",
                            "functionName": "View",
                            "functionMethod": "GET,POST,PUT,UPDATE,DELETE",
                            "functionPath": "/external/510040000",
                            "parentCode": "5100"
                        }
                    ],
                    icon: "next_week",
                    url: "list-employee"
                }
            ],
            token: "123ABC"
        }
    }
}