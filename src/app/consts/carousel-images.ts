import { Carousel } from "../models/carousel.model";

export const CarouselImages: Carousel[] = [
    {
        imageSrc: "../../assets/images/carousel/foto1.jpg",
        imageAlt: "Carousel image One"
    },
    {
        imageSrc: "../../assets/images/carousel/foto2.jpg",
        imageAlt: "Carousel image Two"
    },
    {
        imageSrc: "../../assets/images/carousel/foto3.jpg",
        imageAlt: "Carousel image Three"
    }
];