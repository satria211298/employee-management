export interface Menu {
    menuCode: string;
    menuName: string;
    details: MenuDetails[];
    icon: string;
    url: string;
    children?: Menu[];
}

interface MenuDetails {
    functionCode: string;
    functionMethod: string;
    functionName: string;
    functionPath: string;
    parentCode: string;
}