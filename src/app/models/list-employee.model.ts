export interface listEmployee {
    no: any;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    birthDate: Date;
    basicSallary: string;
    status: string;
    group: string;
    description: string;
  }