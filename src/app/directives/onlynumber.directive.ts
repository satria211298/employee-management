import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appOnlynumber]',
})
export class OnlynumberDirective {
  constructor(private _el: ElementRef) {}

  @HostListener('input', ['$event'])
  onInputChange(event: any) {
    const input = this._el.nativeElement as HTMLInputElement;
    const inputValue = input.value;
    const alphabetsOnly = inputValue.replace(/[^0-9]/g, '');

    if (inputValue !== alphabetsOnly) {
      input.value = alphabetsOnly;
      event.target.value = alphabetsOnly;
      event.preventDefault();
      event.stopPropagation();
      this.emitInputChangeEvent(input, alphabetsOnly);
    }
  }

  @HostListener('paste', ['$event'])
  onPaste(event: ClipboardEvent) {
    event.preventDefault();
    const clipboardData = event.clipboardData || (window as any).clipboardData;
    const pastedText = clipboardData.getData('text/plain');
    const alphabetsOnly = pastedText.replace(/[^0-9]/g, '');

    document.execCommand('insertText', false, alphabetsOnly);
  }

  private emitInputChangeEvent(input: HTMLInputElement, value: string) {
    const inputEvent = new Event('input', { bubbles: true });
    input.value = value;
    input.dispatchEvent(inputEvent);
  }
}
