import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthLoginGuard } from './pages/auth/auth-login.guard';
import { AuthDashboardGuard } from './pages/dashboard/auth-dashboard.guard';
import { ListEmployeeComponent } from './pages/components/list-employee/list-employee.component';
import { AddEmployeeComponent } from './pages/components/add-employee/add-employee.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'dashboard', pathMatch: 'full'
  },
  {
    path: 'auth',
    canActivate: [AuthLoginGuard],
    loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'dashboard',
    canActivate: [AuthDashboardGuard],
    loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path:'list-employee', component:ListEmployeeComponent
  },
  {
    path:'add-employee', component:AddEmployeeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
